ActiveAdmin.register Post do
		permit_params :title, :body, :image

		show do |t|
			attributes_table do
				row :title
				row :body
				row :image do
					post.image? ? image_tag(post.image.url, heigh:'100') : content_tag(:span, "No Photo")
				end
			end
		end

		form :text => { :enctype => "multipart/form-data"} do |f|
			f.inputs do
				f.input :title
				f.input	:body
				f.input :image, hint: post.image? ? image_tag(post.image.url, heigh:'100') : content_tag(:span, "No Photo")
			end
			f.actions
		end

end
