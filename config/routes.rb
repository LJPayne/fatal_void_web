Rails.application.routes.draw do
  
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users
  resources :posts
  resources :home, only: [:index]
  resources :about_team, only: [:index]
  root "home#index"
end
